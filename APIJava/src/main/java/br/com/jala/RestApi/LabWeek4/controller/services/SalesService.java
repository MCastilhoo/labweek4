package br.com.jala.RestApi.LabWeek4.controller.services;


import br.com.jala.RestApi.LabWeek4.factories.SaleFactory;
import br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs.SaleDetailsResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs.SaleRequestDTO;
import br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs.SaleResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.entities.ProductEntity;
import br.com.jala.RestApi.LabWeek4.model.entities.SalesEntity;
import br.com.jala.RestApi.LabWeek4.repositories.ProductRepository;
import br.com.jala.RestApi.LabWeek4.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SalesService {
    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductService productService;


    public SaleResponseDTO create(List<SaleRequestDTO> dto) {
        List<SaleDetailsResponseDTO> responseDTOList = new ArrayList<>();
        boolean allProductsAvailable = true;

        for (SaleRequestDTO requestDTO : dto) {
            Integer productId = requestDTO.productId();
            Integer requestedQuantity = requestDTO.soldAmount();

            boolean productAvailable = productService.checkProductAvailability(productId, requestedQuantity);

            if (!productAvailable) {
                allProductsAvailable = false;
                throw new IllegalArgumentException("Unfortunately the product is out of stock, I apologize for the inconvenience");
            }
        }

        if (allProductsAvailable) {
            for (SaleRequestDTO requestDTO : dto) {
                Integer productId = requestDTO.productId();
                Integer requestedQuantity = requestDTO.soldAmount();

                Optional<ProductEntity> productOptional = productRepository.findById(productId);
                if (productOptional.isPresent()) {
                    ProductEntity product = productOptional.get();

                    int currentStock = product.getQuantityStock();
                    if (currentStock >= requestedQuantity) {
                        // Aplicar descontos progressivos
                        float discountPercentage = 0.0f;
                        String discountString = "";
                        if (requestedQuantity > 10 && requestedQuantity <= 20) {
                            discountPercentage = 0.05f; // 5% de desconto
                            discountString = "5%";
                        } else if (requestedQuantity > 20) {
                            discountPercentage = 0.10f; // 10% de desconto
                            discountString = "10%";
                        }

                        float price = product.getPrice() * requestedQuantity;
                        float discountAmount = price * discountPercentage;
                        float totalPrice = price - discountAmount;

                        product.setQuantityStock(currentStock - requestedQuantity);
                        productRepository.save(product);

                        SalesEntity sale = new SalesEntity();
                        sale.setProduct(product);
                        sale.setSoldAmount(requestedQuantity);
                        sale.setPurchaseValue(totalPrice);
                        sale.setSellDate(LocalDateTime.now());
                        sale.setDiscountApplied(discountPercentage != 0.0f ? discountString : null); // Se o desconto for aplicado, armazena o valor, caso contrário, armazena null

                        saleRepository.save(sale);

                        SaleDetailsResponseDTO saleDetailsResponseDTO = new SaleDetailsResponseDTO(
                                product,
                                requestedQuantity,
                                totalPrice,
                                LocalDateTime.now()
                        );
                        responseDTOList.add(saleDetailsResponseDTO);
                    }
                }
            }
            throw new IllegalArgumentException("successful sale");
        }
        return null;
    }




    public List<SaleDetailsResponseDTO> getAll (Pageable pageable){
            Page<SalesEntity> sales = saleRepository.findAll(pageable);

            List<SaleDetailsResponseDTO> results = sales
                    .stream()
                    .map(SaleFactory::CreatedDetails)
                    .collect(Collectors.toList());

            return results;
        }

        public SaleResponseDTO getById (Integer id){
            Optional<SalesEntity> result = saleRepository.findById(id);
            if (result.isEmpty()) return null;
            return new SaleResponseDTO();
        }
}

