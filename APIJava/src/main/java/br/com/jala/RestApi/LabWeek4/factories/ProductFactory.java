package br.com.jala.RestApi.LabWeek4.factories;

import br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs.ProductDetailsResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.entities.ProductEntity;

public class ProductFactory {
    public static ProductDetailsResponseDTO CreateDetails(ProductEntity product){
        return new ProductDetailsResponseDTO(
                product.getProductId(),
                product.getProductName(),
                product.getPrice(),
                product.getQuantityStock(),
                product.getCreatedAt(),
                product.getUpdatedAt()

        );
    }
}
