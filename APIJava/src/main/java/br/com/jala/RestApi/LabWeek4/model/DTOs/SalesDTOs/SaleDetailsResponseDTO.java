package br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs;

import br.com.jala.RestApi.LabWeek4.model.entities.ProductEntity;

import java.io.Serializable;
import java.time.LocalDateTime;


public record SaleDetailsResponseDTO(
        ProductEntity product,
        Integer soldAmount,
        Float purchaseValue,
        LocalDateTime sellDate
) implements Serializable {
}
