package br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record ProductRequestDTO(
        Float price,
        @NotBlank String productName,

        @NotNull Integer quantityStock
) {}