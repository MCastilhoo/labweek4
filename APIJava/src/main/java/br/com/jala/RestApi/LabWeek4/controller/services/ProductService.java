package br.com.jala.RestApi.LabWeek4.controller.services;
import br.com.jala.RestApi.LabWeek4.factories.ProductFactory;
import br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs.ProductDetailsResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs.ProductRequestDTO;
import br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs.ProductResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.entities.ProductEntity;
import br.com.jala.RestApi.LabWeek4.repositories.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductResponseDTO create(ProductRequestDTO requestDto) {
        if (requestDto.price() == null || requestDto.price() <= 0) {
            throw new IllegalArgumentException("Price must be provided and greater than zero");
        }

        if (requestDto.quantityStock() == null || requestDto.quantityStock() <= 0) {
            throw new IllegalArgumentException("Quantity stock must be provided and greater than zero");
        }

        ProductEntity product = new ProductEntity();
        BeanUtils.copyProperties(requestDto, product);

        ProductEntity result = productRepository.save(product);

        return new ProductResponseDTO(result);
    }


    public ProductResponseDTO getById(Integer id){
        Optional<ProductEntity> result = productRepository.findById(id);
        if (result.isEmpty()) return null;
        return new ProductResponseDTO(result.get());
    }

    public List<ProductDetailsResponseDTO> getAll(Pageable pageable){
        Page<ProductEntity> product = productRepository.findAll(pageable);

        List<ProductDetailsResponseDTO> results = product
                .stream()
                .map(ProductFactory::CreateDetails)
                .collect(Collectors.toList());

        return results;
    }

    public ProductResponseDTO update(ProductRequestDTO requestDto, Integer id){
        Optional<ProductEntity> result = productRepository.findById(id);

        if (result.isEmpty()) return null;

        result.get().setProductName(requestDto.productName());
        result.get().setPrice(requestDto.price());
        result.get().setQuantityStock(requestDto.quantityStock());

        ProductEntity saved = productRepository.save(result.get());

        return new ProductResponseDTO(saved);
    }

    public boolean delete(Integer id){
        Optional<ProductEntity> result = productRepository.findById(id);

        if (result.isEmpty()) return false;

        productRepository.delete(result.get());

        return true;
    }

    public boolean checkProductAvailability(Integer id, Integer requestedQuantity){
        Optional<ProductEntity>productOptional = productRepository.findById(id);
        if (productOptional.isPresent()){
            ProductEntity product = productOptional.get();
            return product.getQuantityStock() >= requestedQuantity;
        } else {
            return false;
        }
    }
}
