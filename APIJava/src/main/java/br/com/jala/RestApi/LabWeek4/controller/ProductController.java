package br.com.jala.RestApi.LabWeek4.controller;


import br.com.jala.RestApi.LabWeek4.controller.services.ProductService;
import br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs.ProductDetailsResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs.ProductRequestDTO;
import br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs.ProductResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.entities.ProductEntity;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Pageable;
import java.util.List;


@RestController
@RequestMapping("/rest/api/products")
public class ProductController {
    @Autowired
    private ProductService service;

    @PostMapping()
    public ResponseEntity<ProductResponseDTO>create(@RequestBody @Valid ProductRequestDTO dto){
        ProductResponseDTO responseDTO = service.create(dto);

        return ResponseEntity.status(HttpStatus.CREATED).body(responseDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDTO> get(@PathVariable(value = "id")Integer id){
        ProductResponseDTO result = service.getById(id);

        if (result == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @GetMapping
    public ResponseEntity<List<ProductDetailsResponseDTO>> getAll(@PageableDefault(page = 0, size = 9)Pageable pageable){
        return ResponseEntity.status(HttpStatus.OK).body(service.getAll(pageable));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductResponseDTO> update(@PathVariable(value = "id") Integer id, @RequestBody @Valid ProductRequestDTO dto){
        return ResponseEntity.status(HttpStatus.OK).body(service.update(dto, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ProductEntity> delete(@PathVariable(value = "id") Integer id){
        if (service.delete(id)){
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
