package br.com.jala.RestApi.LabWeek4.model.entities;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "SALES")
public class SalesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Integer sale_id;

    @Getter
    @ManyToOne
    @JoinColumn(name = "product_id")
    private ProductEntity product;

    @Column(name = "purchase_value")
    Float purchaseValue;
    @Column(name = "sold_amount")
    Integer soldAmount;
    @Column(name = "discount_applied")
    String discountApplied;
    @Column(name = "sell_date")
    LocalDateTime sellDate;


}