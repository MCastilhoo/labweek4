package br.com.jala.RestApi.LabWeek4.repositories;

import br.com.jala.RestApi.LabWeek4.model.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository <ProductEntity, Integer>{

}
