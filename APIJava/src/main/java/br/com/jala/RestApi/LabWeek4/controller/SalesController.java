package br.com.jala.RestApi.LabWeek4.controller;


import br.com.jala.RestApi.LabWeek4.controller.services.SalesService;
import br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs.SaleDetailsResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs.SaleRequestDTO;
import br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs.SaleResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Pageable;

import java.util.List;

@RestController
@RequestMapping("/rest/api/sales")
public class SalesController {
    @Autowired

    private SalesService service;

    @PostMapping
    public ResponseEntity<SaleResponseDTO>create(@RequestBody List<SaleRequestDTO> dto){
        SaleResponseDTO responseDTO = service.create(dto);

        return ResponseEntity.status(HttpStatus.CREATED).body(responseDTO);
    }

    @GetMapping
    public ResponseEntity<List<SaleDetailsResponseDTO>> getAll(@PageableDefault(page = 0, size = 3)Pageable pageable){
        return ResponseEntity.status(HttpStatus.OK).body(service.getAll(pageable));
    }

    @GetMapping("{id}")
    public ResponseEntity<SaleResponseDTO> get(@PathVariable(value = "id") Integer id){
        SaleResponseDTO result = service.getById(id);

        if (result == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
}
