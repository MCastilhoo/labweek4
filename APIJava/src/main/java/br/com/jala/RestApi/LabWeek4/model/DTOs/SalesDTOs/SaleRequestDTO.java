package br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;



public record SaleRequestDTO(

        @NotNull Integer productId,
        @NotNull @Positive Integer soldAmount

) {}
