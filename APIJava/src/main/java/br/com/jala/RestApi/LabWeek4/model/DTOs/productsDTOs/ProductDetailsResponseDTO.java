package br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs;

import java.io.Serializable;
import java.time.LocalDateTime;

public record ProductDetailsResponseDTO(

        Integer productId,
        String productName,
        Float price,
        Integer stockQuantity,
        LocalDateTime createdAt,
        LocalDateTime updateAt

) implements Serializable {
}
