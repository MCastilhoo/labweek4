package br.com.jala.RestApi.LabWeek4.repositories;

import br.com.jala.RestApi.LabWeek4.model.entities.SalesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleRepository extends JpaRepository<SalesEntity, Integer> {

}
