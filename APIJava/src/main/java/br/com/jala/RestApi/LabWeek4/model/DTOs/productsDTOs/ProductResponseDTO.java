package br.com.jala.RestApi.LabWeek4.model.DTOs.productsDTOs;

import br.com.jala.RestApi.LabWeek4.model.entities.ProductEntity;

import java.io.Serializable;

public record ProductResponseDTO(ProductEntity productEntity) implements Serializable {}