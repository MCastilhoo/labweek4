package br.com.jala.RestApi.LabWeek4.factories;

import br.com.jala.RestApi.LabWeek4.model.DTOs.SalesDTOs.SaleDetailsResponseDTO;
import br.com.jala.RestApi.LabWeek4.model.entities.SalesEntity;


public class SaleFactory {
    public static SaleDetailsResponseDTO CreatedDetails(SalesEntity sales){
        return new SaleDetailsResponseDTO(
                sales.getProduct(),
                sales.getSoldAmount(),
                sales.getPurchaseValue(),
                sales.getSellDate()
        );
    }
}
