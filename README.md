
# Documentação da API REST para Loja de Eletrônicos Online 🛒

Este é um projeto de API para uma loja de eletrônicos online, construída com o framework Spring Boot. Ele proporciona uma gama de funcionalidades essenciais para a administração de produtos e vendas em uma plataforma de compras online. Abaixo, será fornecida uma descrição abrangente das características principais, das tecnologias empregadas e das diretrizes para a execução do projeto.

## Funcionalidades 💻
- Obter todos os produtos
- Obter dados de um produto específico 
- Criar um novo produto no sistema
- Atualizar um produto
- Excluir um produto do banco de dados
- Realizar uma vendas
- Verificar histórico de vendas

## Rotas 📂

### Produtos
- `GET /rest/api/products`: Retorna todos os produtos.
- `GET /rest/api/products/{id}`: Retorna um produto com o ID específico.
- `POST /rest/api/products`: Cria um novo produto.
- `PUT /rest/api/products/{id}`: Atualiza um produto existente com o ID especificado
- `DELETE /rest/api/products/{id}`: Exclui um produto com o ID especificado

### vendas
- `GET /rest/api/sales`: Retorna todas as vendas realizadas
- `POST /rest/api/sales`: Reliza uma nova venda

![App Screenshot](./assets/Screenshot_1.png)



## Tecnologias Utilizadas ⚙️
- **Spring Boot com Java 17:** Framework de desenvolvimento para aplicativos Java.
- **Srping Data JPA:** Facilita o acesso e manipulação de dados em bancos de dados relacionais.
- **Lombok:** Fornece um conjunto de anotações e ferramentas que ajudam os desenvolvedores a reduzir a quantidade de código "boilerplate" (repetitivo) que eles precisam escrever
- **MySQL:** Sistema de gerencimento de banco de dados relacional.
- **Swagger:** Ferramenta que auxilia na documentação e exposição de APIs REST.

## Estrutura do Projeto 🛠️ 
O projeto está estruturado seguindo o padrão de arquitetura de software MVC (Model-View-Controller), com os pacotes organizados de forma a separar as responsabilidades de cada componente:


- **`br.com.jala.RestApi.LabWeek4.controller`**: Este pacote abriga as classes de controladores que gerenciam as solicitações HTTP, fornecendo uma separação clara entre a lógica de negócios e a camada de apresentação da aplicação.
- **`br.com.jala.RestApi.LabWeek4.service`**: Aqui estão as classes de serviço responsáveis por implementar a lógica central da aplicação, garantindo que ela permaneça isolada tanto da camada de apresentação quanto da camada de persistência de dados.
- **`br.com.jala.RestApi.LabWeek4.model`**: Este pacote define as entidades de negócios da aplicação, assegurando que a representação dos dados permaneça separada das regras de negócios para uma manutenção mais eficiente.
- **`br.com.jala.RestApi.LabWeek4.repositories`**: Neste pacote, são definidas as interfaces que estendem JpaRepository para facilitar a interação com o banco de dados, mantendo uma distinção clara entre as operações de banco de dados e a lógica de negócios da aplicação.

## Executando o Projeto ⌨️
Para executar esta aplicação, será necessário configurar a URL e os dados de conexão do seu banco de dados, e criar um banco de dados com o nome de "market". Ao iniciar a aplicação, o Hibernate irá criar todas as tabelas necessárias para a execução do projeto.

## Documentação da API 📋
A documentação do projeto pode ser acessada atráves do Swagger UI. Após iniciar a API, você conseguirá acessar o seguinte URL em qualquer navegador do seu computador:

**[Swagger UI ](http://localhost:8080/swagger-ui/index.html#/)**


